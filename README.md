# ansible-lab-with-terraform

Ansible Lab on AWS making with Terraform

This lab creates five Centos 8 Virtual Machines.

One VM configured as an Ansible control node, and the others were configured to apply the playbooks.

## Requirements
* AWS Account with CLI credentials configured
* Terraform <= 1.0

## VM Creation

Creation of five virtual machines. 

```bash
terraform apply
```

## Ansible Bootstrap

Copy SSH key, connect to the control-node and execute the bootstrap playbook

```
scp ~/.ssh/id_rsa ec2-user@ec2-xx-xxx-xxx-x.compute-1.amazonaws.com:~/.ssh
ssh ec2-user@ec2-xx-xxx-xxx-x.compute-1.amazonaws.com
cd ansible-lab-on-aws-main && ansible-playbook bootstrap-ansible.yaml
```
