provider "aws" {
  region = var.env["region"]
}

#resource "null_resource" "copy_key" {
# provisioner "local-exec" {
#    command = "scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/id_rsa.pub ec2-user@${module.ansible_control_node.public_dns}:~/.ssh/id_rsa.pub ."
#  }
#}

module "aws_key_pair" {
  source              = "cloudposse/key-pair/aws"
  #source              = "git@gitlab.com:naguer/tf-aws-key-pair.git"
  # version             = "0.16.1"
  namespace           = var.env["namespace"]
  stage               = var.env["stage"]
  name                = var.env["name"]
  # ssh_public_key_file = "id_rsa.pub"
  # ssh_public_key_path = "~/.ssh"
  ssh_public_key_path = "./secrets"
  generate_ssh_key = true
}

module "ansible_control_node" {
  source                      = "git@github.com:cloudposse/terraform-aws-ec2-instance.git?ref=0.33.0"
  name                        = var.ec2["name_control_node"]
  ssh_key_pair                = module.aws_key_pair.key_name
  vpc_id                      = var.ec2["vpc_id"]
  subnet                      = var.ec2["subnet"]
  security_groups             = [aws_security_group.ansible_ssh.id]
  assign_eip_address          = var.ec2["assign_eip_address"]
  associate_public_ip_address = var.ec2["associate_public_ip_address"]
  instance_type               = var.ec2["instance_type"]
  ami                         = var.ec2["ami"]
  ami_owner                   = var.ec2["ami_owner"]
  # user_data                   = "${file("install-ansible.sh")}"
  user_data                   = data.template_file.bootstrap_ansible.rendered
  instance_profile            = aws_iam_instance_profile.ec2_read_only.name

}

module "ansible_node_0" {
  source                      = "git@github.com:cloudposse/terraform-aws-ec2-instance.git?ref=0.33.0"
  name                        = var.ec2["name_node_0"]
  ssh_key_pair                = module.aws_key_pair.key_name 
  vpc_id                      = var.ec2["vpc_id"]
  subnet                      = var.ec2["subnet"]
  security_groups             = [aws_security_group.ansible_ssh.id]
  assign_eip_address          = var.ec2["assign_eip_address"]
  associate_public_ip_address = var.ec2["associate_public_ip_address"]
  instance_type               = var.ec2["instance_type"]
  ami                         = var.ec2["ami"]
  ami_owner                   = var.ec2["ami_owner"]
  user_data                   = data.template_file.bootstrap_ansible.rendered
  instance_profile            = aws_iam_instance_profile.ec2_read_only.name

}

module "ansible_node_1" {
  source                      = "git@github.com:cloudposse/terraform-aws-ec2-instance.git?ref=0.33.0"
  name                        = var.ec2["name_node_1"]
  ssh_key_pair                = module.aws_key_pair.key_name 
  vpc_id                      = var.ec2["vpc_id"]
  subnet                      = var.ec2["subnet"]
  security_groups             = [aws_security_group.ansible_ssh.id]
  assign_eip_address          = var.ec2["assign_eip_address"]
  associate_public_ip_address = var.ec2["associate_public_ip_address"]
  instance_type               = var.ec2["instance_type"]
  ami                         = var.ec2["ami"]
  ami_owner                   = var.ec2["ami_owner"]
  user_data                   = data.template_file.bootstrap_ansible.rendered
  instance_profile            = aws_iam_instance_profile.ec2_read_only.name

}

module "ansible_node_2" {
  source                      = "git@github.com:cloudposse/terraform-aws-ec2-instance.git?ref=0.33.0"
  name                        = var.ec2["name_node_2"]
  ssh_key_pair                = module.aws_key_pair.key_name 
  vpc_id                      = var.ec2["vpc_id"]
  subnet                      = var.ec2["subnet"]
  security_groups             = [aws_security_group.ansible_ssh.id]
  assign_eip_address          = var.ec2["assign_eip_address"]
  associate_public_ip_address = var.ec2["associate_public_ip_address"]
  instance_type               = var.ec2["instance_type"]
  ami                         = var.ec2["ami"]
  ami_owner                   = var.ec2["ami_owner"]
  user_data                   = data.template_file.bootstrap_ansible.rendered
  instance_profile            = aws_iam_instance_profile.ec2_read_only.name

}

module "ansible_node_3" {
  source                      = "git@github.com:cloudposse/terraform-aws-ec2-instance.git?ref=0.33.0"
  name                        = var.ec2["name_node_3"]
  ssh_key_pair                = module.aws_key_pair.key_name 
  vpc_id                      = var.ec2["vpc_id"]
  subnet                      = var.ec2["subnet"]
  security_groups             = [aws_security_group.ansible_ssh.id]
  assign_eip_address          = var.ec2["assign_eip_address"]
  associate_public_ip_address = var.ec2["associate_public_ip_address"]
  instance_type               = var.ec2["instance_type"]
  ami                         = var.ec2["ami"]
  ami_owner                   = var.ec2["ami_owner"]
  user_data                   = data.template_file.bootstrap_ansible.rendered
  instance_profile            = aws_iam_instance_profile.ec2_read_only.name

}

resource "aws_security_group" "ansible_ssh" {
  name        = "ec2_ssh"
  description = "Open SSH"
  vpc_id      = var.ec2["vpc_id"]

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "Open SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    description = "Allow outbound traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_role" "ec2_read_only" {
  name               = "ec2-read-only"
  assume_role_policy = file("ec2_assume_policy.json")
}

resource "aws_iam_role_policy_attachment" "ec2_read_only" {
  role       = aws_iam_role.ec2_read_only.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "ssm_instance" {
  role       = aws_iam_role.ec2_read_only.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_instance_profile" "ec2_read_only" {
  name = aws_iam_role.ec2_read_only.name 
  role = aws_iam_role.ec2_read_only.name
}

data "template_file" "bootstrap_ansible" {

  template = file("install-ansible.sh")

  vars = {
    private_key = module.aws_key_pair.private_key
  }
}
