output "public_key_filename" {
  description = "Public Key Filename"
  value       = module.aws_key_pair.public_key_filename
}

output "private_key" {
  description = "Public Key Filename"
  value       = module.aws_key_pair.private_key
}

output "public_dns_ansible_control_node" {
  description = "Public DNS of instance (or DNS of EIP)"
  value       = module.ansible_control_node.public_dns
}

output "public_dns_ansible_node_0" {
  description = "Public DNS of instance (or DNS of EIP)"
  value       = module.ansible_node_0.public_dns
}
