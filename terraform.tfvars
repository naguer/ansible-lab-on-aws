env = {
  namespace = "nah"
  stage     = "dev"
  name      = "ansible"
  region    = "us-east-1"
}

ec2 = {
  name_control_node           = "control-node"
  name_node_0                 = "node-0"
  name_node_1                 = "node-1"
  name_node_2                 = "node-2"
  name_node_3                 = "node-3"
  assign_eip_address          = false
  associate_public_ip_address = true
  instance_type               = "t3.micro"
  vpc_id                      = "vpc-e917e194"
  subnet                      = "subnet-5c4d3811"
  # Amazon Linux 2 
  ami       		      = "ami-0d5eff06f840b45e9"
  ami_owner                   = "amazon"
  # RHEL 8
  # ami                       = "ami-09e23e054089857d9"
  # ami_owner                 = "679593333241"
}
